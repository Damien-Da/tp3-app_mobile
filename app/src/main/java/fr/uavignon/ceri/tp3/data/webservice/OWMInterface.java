package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMInterface {
    @GET("weather")
    public Call<WeatherResponse> getWeather(@Query("q") String query,
                                            @Query("APIkey") String apiKey);
}
//cc2b12b27eccf77d69690f264a280b5b