package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private WeatherRepository repository;
    private MutableLiveData<City> city;

    private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<Boolean> isLoading; // = new MutableLiveData<>();

    public DetailViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        city = new MutableLiveData<>();
        isLoading = repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
    }

    LiveData<City> getCity() {
        return city;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    void resetWebServiceThrowable() {
        repository.resetWebServiceThrowable();
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    LiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void loadWeatherCity() {
        repository.loadWeatherCity(city.getValue());
    }
}

